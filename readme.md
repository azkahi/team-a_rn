# This is description for Team A React Native

### Here are members of Team A:

FRONTEND MEMBER

Azka Hanif Imtiyaz (Stack Lead - AND)
Furqon Ahkamy (Developer)
Imam Abdul Hakim (A) (Stack Lead - iOS)
Rizal Antori (Developer)

BACKEND MEMBER

Asep Jajang Nurjaya (Maintainer - Stack Lead)
Dona Saputra (Developer)
Fajar Wahyu Ardianto (Developer)
Fajri Rahmanda (Maintainer)
Stephanus Krisna Arizona Widiputra (Developer)
Handoko Darmawan Wicaksono (Developer)

---

### Design

[Design mockup ](https://marvelapp.com/prototype/56f3cb5/screens)

---

### Installation

Clone project and install the dependencies.

```sh
$ git clone https://gitlab.com/binar-telkomsel_use-case-project/team-a_dsc-corporate-pic-apps/team-a_rn.git
$ cd team-a_rn
$ npm install
```

### Dependencies

Team A-RN uses several dependencies to support its functionality.

| Plugin                                    | README |
| ----------------------------------------- | ------ |
| @react-native-async-storage/async-storage | -      |
| @react-native-community/masked-view       | -      |
| @react-navigation/bottom-tabs             | -      |
| @react-navigation/drawer                  | -      |
| @react-navigation/material-bottom-tabs    | -      |
| @react-navigation/material-top-tabs       | -      |
| @react-navigation/native                  | -      |
| @react-navigation/stack                   | -      |
| axios                                     | -      |
| lottie-ios                                | -      |
| lottie-react-native                       | -      |
| react                                     | -      |
| react-native                              | -      |
| react-native-chart-kit                    | -      |
| react-native-elements                     | -      |
| react-native-flipper                      | -      |
| react-native-gesture-handler              | -      |
| react-native-image-picker                 | -      |
| react-native-material-color               | -      |
| react-native-paper                        | -      |
| react-native-reanimated                   | -      |
| react-native-safe-area-context            | -      |
| react-native-screens                      | -      |
| react-native-svg                          | -      |
| react-native-tab-view                     | -      |
| react-native-vector-icons                 | -      |
| react-redux                               | -      |
| reanimated-bottom-sheet                   | -      |
| redux                                     | -      |
| redux-devtools-extension                  | -      |
| redux-flipper                             | -      |
| redux-promise-middleware                  | -      |
| redux-thunk                               | -      |

#### Running for IoS

```sh
$ npx react-native run-ios
```

#### Running for Android

```sh
$ npx react-native run-android
```

---
