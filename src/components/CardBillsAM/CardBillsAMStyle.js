import { StyleSheet } from 'react-native';
import { Colors } from 'utils/ColorReferences';

export const styles = StyleSheet.create({
  cardInvoice: {
    backgroundColor: Colors.white700,
    borderColor: Colors.gray700,
    borderWidth: 0.5,
    borderRadius: 5,
    marginHorizontal: 15,
    marginTop: 10,
    flexDirection: 'row'
  },

  invoiceWrap: {
    marginLeft: 10,
    marginVertical: 10
  },
  invoice: {
    color: Colors.red900,
    fontWeight: 'bold'
  },
  month: {
    color: Colors.red700,
    fontWeight: 'bold',
    marginVertical: 2
  },
  amount: {
    fontWeight: 'bold',
    color: Colors.gray700
  },
  statusWrap: {
    alignSelf: 'center',
    marginLeft: 80,
    flexDirection: 'row'
  },
  status: {
    textAlign: 'center',
    color: Colors.red700,
    fontWeight: 'bold',
    lineHeight: 30
  },
  icon: {
    marginLeft: 30
  }
});
