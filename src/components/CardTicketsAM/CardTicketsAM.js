import React from 'react';
import { Text, View } from 'react-native';

import { styles } from './CardTicketsAMStyle';

const CardTicketsAM = (props) => {
  return (
    <View style={styles.cardTickets}>
      <View style={styles.headWrap}>
        <View style={styles.titleHead}>
          <Text style={styles.title}>Internet problem</Text>
          <Text style={styles.subtitle}>Can't connect</Text>
        </View>
      </View>

      <View style={styles.ticketStatus}>
        <Text style={styles.textTicket}>
          When the Internet connection issue is solved
        </Text>
        <View style={styles.statusWrap}>
          <Text style={styles.status}>Status:</Text>
          <Text style={styles.status}>In Progress</Text>
        </View>
      </View>
      <Text style={styles.company}>PT. Fakhaza Healthy Kitchen</Text>
    </View>
  );
};

export default CardTicketsAM;
