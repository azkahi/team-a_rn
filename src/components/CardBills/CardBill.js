import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { Colors } from '../../utils/ColorReferences';

import { styles } from './CardBillStyle';

const CardBill = ({ onPress, date, total, status, companyName, disabled }) => {
  return (
    <TouchableOpacity
      style={styles.cardBills}
      onPress={onPress}
      disabled={disabled}
    >
      <View>
        <Icon
          name="cash-multiple"
          style={styles.icon}
          size={40}
          color={Colors.red900}
        />
      </View>

      <View style={styles.headMonth}>
        { date && <Text style={styles.textMonth}>{date}</Text> }
        { companyName && <Text style={styles.textMonth}>{companyName}</Text> }
        <Text style={styles.textMonth}>Rp. {total}</Text>
        { status && <Text style={styles.textMonth}>Status: {status}</Text> }
      </View>
    </TouchableOpacity>
  );
};

export default CardBill;
