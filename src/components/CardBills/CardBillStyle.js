import { StyleSheet } from 'react-native';
import { Colors } from 'utils/ColorReferences';

export const styles = StyleSheet.create({
  cardBills: {
    flexDirection: 'row',
    backgroundColor: Colors.white700,
    height: 90,
    borderRadius: 5,
    borderWidth: 0.5,
    borderColor: Colors.gray700,
    paddingRight: 10,
    paddingBottom: 20,
    marginLeft: 20
  },
  icon: {
    padding: 5,
    marginLeft: 5
  },
  details: {
    color: 'blue',
    marginLeft: 10,
    marginTop: 10,
    fontSize: 16
  },
  headMonth: {
    marginLeft: 10,
    marginTop: 10
  },
  textMonth: {
    color: Colors.red900,
    fontSize: 14
  }
});
