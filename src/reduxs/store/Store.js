import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { persistStore, persistReducer } from 'redux-persist';

import AuthReducer from 'reduxs/reducers/AuthReducer';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['session']
};

const rootReducer = combineReducers({
  AuthReducer: persistReducer(persistConfig, AuthReducer)
});

const middlewares = [thunk];
if (__DEV__) {
  const createDebugger = require('redux-flipper').default;
  middlewares.push(createDebugger());
}

export const store = createStore(rootReducer, applyMiddleware(...middlewares));
export const persistor = persistStore(store);

// unused Redux Thunk
// import { createStore, applyMiddleware } from "redux";
// import thunk from "redux-thunk";
// import promise from "redux-promise-middleware";
// import rootReducer from "./Reducers.js";

// const middlewares = [promise, thunk];

// if (__DEV__) {
//   const createDebugger = require("redux-flipper").default;
//   middlewares.push(createDebugger());
// }

// const configureStore = createStore(
//   rootReducer,
//   applyMiddleware(...middlewares)
// );

// export default configureStore;
