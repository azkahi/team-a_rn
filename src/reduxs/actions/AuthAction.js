export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_LOADING = 'LOGIN_LOADING';
export const LOGIN_FAILED = 'LOGIN_FAILED';
export const STORE_DATA_SUCCESS = 'STORE_DATA_SUCCESS';
export const STORE_DATA_LOADING = 'STORE_DATA_LOADING';
export const STORE_DATA_FAILED = 'STORE_DATA_FAILED';
export const LOGOUT_ACTION = 'LOGOUT_ACTION';

import { doLogin } from 'engine/ApiProcessors';
export const reduxLogin = (param) => {
  try {
    return async (dispatch) => {
      dispatch({
        type: LOGIN_LOADING,
        isLoading: true
      });
      let res = await doLogin(param);
      // console.log(res);
      if (res) {
        if (res.data.status === true) {
          let data = {
            isLoggedIn: true,
            token: `Bearer ${res.data.data.token}`,
            role: res.data.data.role,
            data: res.data.data
          };
          dispatch({
            type: LOGIN_SUCCESS,
            isLoading: false,
            payload: data
          });
        } else {
          dispatch({
            type: LOGIN_FAILED,
            isLoading: false
          });
        }
      } else {
        dispatch({
          type: LOGIN_FAILED,
          isLoading: false
        });
      }
    };
  } catch (error) {
    dispatch({
      type: LOGIN_FAILED,
      isLoading: false
    });
  }
};

import { doUpdateProfile } from 'engine/ApiProcessors';
export const reduxUpdateProfile = (role, param, token) => {
  try {
    return async (dispatch) => {
      dispatch({
        type: STORE_DATA_LOADING,
        isLoading: true
      });
      let res = await doUpdateProfile(role, param, token);
      // console.log(res);
      if (res) {
        if (res.data.status === true) {
          let data = {
            isLoggedIn: true,
            token: token,
            role: role,
            data: res.data.data
          };
          dispatch({
            type: STORE_DATA_SUCCESS,
            isLoading: false,
            payload: data
          });
        } else {
          dispatch({
            type: STORE_DATA_FAILED,
            isLoading: false
          });
        }
      } else {
        dispatch({
          type: STORE_DATA_FAILED,
          isLoading: false
        });
      }
    };
  } catch (error) {
    dispatch({
      type: STORE_DATA_FAILED,
      isLoading: false
    });
  }
};

export const reduxLogout = () => (dispatch) => {
  dispatch({
    type: LOGOUT_ACTION
  });
};
