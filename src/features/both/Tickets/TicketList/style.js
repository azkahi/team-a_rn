import { StyleSheet } from 'react-native';
import { Colors } from 'utils/ColorReferences';

export const styles = StyleSheet.create({
  mainPage: {
    backgroundColor: Colors.white700,
    flex: 1
  },
  containerStyle: {
    backgroundColor: Colors.white700,
    borderBottomColor: '#bdc3c7'
  },
  inputContainerStyle: {
    backgroundColor: '#bdc3c7',
    borderRadius: 20,
    height: 40
  },
  inputText: {
    color: Colors.gray700
  },
  filterButton: {
    backgroundColor: '#bdc3c7',
    height: 40,
    width: 100,
    alignSelf: 'center',
    borderRadius: 10,
    flexDirection: 'row',
    position: 'absolute',
    bottom: 50,
    borderColor: Colors.gray700,
    borderWidth: 0.5
  },
  iconFilter: {
    alignSelf: 'center',
    marginHorizontal: 5
  },
  textFilter: {
    alignItems: 'center',
    alignSelf: 'center',
    marginLeft: 10,
    fontSize: 16,
    color: Colors.red900,
    fontWeight: 'bold'
  }
});
