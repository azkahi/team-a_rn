import React, { useState } from 'react';
import { Text, View, FlatList, TouchableOpacity } from 'react-native';

import { SearchBar } from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { styles } from './style';

const TicketsAM = () => {
  const [value, onChangeText] = useState('');
  return (
    <View style={styles.mainPage}>
      <SearchBar
        placeholder="Search Tickets"
        onChangeText={(text) => onChangeText(text)}
        value={value}
        lightTheme
        containerStyle={styles.containerStyle}
        inputContainerStyle={styles.inputContainerStyle}
        style={styles.inputText}
      />

      <FlatList
        data={[
          { name: 'satu' },
          { name: 'dua' },
          { name: 'tiga' },
          { name: 'empat' },
          { name: 'lima' },
          { name: 'enam' }
        ]}
        keyExtractor={(item, index) => 'key' + index}
        renderItem={(item) => {
        }}
      />
      <TouchableOpacity style={styles.filterButton}>
        <Icon
          name="filter-variant"
          style={styles.iconFilter}
          size={25}
        />
        <Text style={styles.textFilter}>Filter</Text>
      </TouchableOpacity>
    </View>
  );
};

export default TicketsAM;
