import { StyleSheet } from 'react-native';
import { Colors } from 'utils/ColorReferences';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.red900
  },
  label: {
    color: 'white',
    paddingVertical: 24,
    paddingHorizontal: 16,
    fontWeight: 'bold',
    fontSize: 28
  }
});
