import React from 'react';
import { useEffect, useState, useRef } from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  Linking
} from 'react-native';

import { useSelector } from 'react-redux';
// import ImagePicker from "react-native-image-crop-picker";
import { Image } from 'react-native';
import { Images } from 'utils/ImageReferences';
import { checkVersion } from 'engine/ApiProcessors';
import { Platform } from 'react-native';
import { AppVersions } from 'utils/AppVersions';
import { Colors } from 'utils/ColorReferences';

import BottomSheet from 'reanimated-bottom-sheet';
import Icon from 'react-native-vector-icons/FontAwesome';

import { styles } from './style';

const SplashScreen = (props) => {
  const { session } = useSelector((state) => state.AuthReducer);

  const [url, setUrl] = useState('https://teama-dsc.web.app/');

  const sheetRef = useRef(null);
  const renderContent = () => (
    <View
      style={{
        backgroundColor: Colors.white,
        padding: 30,
        height: 180,
        width: '100%',
        borderTopLeftRadius: 14,
        borderTopRightRadius: 14
      }}
    >
      <Text
        style={{
          fontWeight: 'bold',
          fontSize: 18
        }}
      >
        Your apps is obsolete, click the button to Update!
      </Text>
      <View
        style={{
          flex: 1,
          width: '100%',
          flexDirection: 'row',
          justifyContent: 'space-around'
        }}
      >
        <TouchableOpacity
          activeOpacity={0.5}
          style={styles.button}
          onPress={() => Linking.openURL(url)}
        >
          <Icon
            name="download"
            size={42}
            color={Colors.red700}
            style={{
              paddingTop: 30
            }}
          />
        </TouchableOpacity>
      </View>
    </View>
  );

  useEffect(() => {
    setTimeout(async () => {
      // dont forget to change version apps when publish the Apps.
      // check file /src/utils/AppVersions.js
      const oss = Platform.OS === 'ios' ? 'ios' : 'android';
      const versions =
        Platform.OS === 'ios' ?
          `${AppVersions.ios_version}`
          : `${AppVersions.android_version}`;
      let param = {
        version: String(versions),
        os: String(oss)
      };
      console.log('param: ' + param);
      try {
        let res = await checkVersion(param);
        console.log(res);
        if (res) {
          if (res.data.status === true) {
            if (session.isLoggedIn === false) {
              props.navigation.reset({
                index: 0,
                routes: [{ name: 'Login' }]
              });
            } else {
              if (session.role === 'telkomsel') {
                props.navigation.reset({
                  index: 0,
                  routes: [{ name: 'TselBottomNavigator' }]
                });
              } else {
                props.navigation.reset({
                  index: 0,
                  routes: [{ name: 'CorpBottomNavigator' }]
                });
              }
            }
          } else {
            setUrl(res.data.data.url);
            sheetRef.current.snapTo(0);
          }
        } else {
          console.log(res);
        }
      } catch (error) {
        console.log(error);
      }
    }, 1000);
  }, [session]);

  return (
    <>
      <View style={styles.container}>
        <Image
          source={Images.app_icon}
          style={{
            width: 120,
            height: 120
          }}
        />
        {/* <TouchableHighlight
        onPress={() => {
          ImagePicker.openPicker({
            width: 600,
            height: 600,
            cropping: true,
            includeBase64: true,
          }).then((image) => {
            console.log(image);
          });
        }}
      > */}
        <Text style={styles.label}>Digital Smart Care</Text>
        {/* </TouchableHighlight> */}
      </View>
      <BottomSheet
        ref={sheetRef}
        initialSnap={2}
        snapPoints={[180, 180, 0]}
        borderRadius={10}
        renderContent={renderContent}
        onCloseEnd={
          () => {
            sheetRef.current.snapTo(0);
          }
        }
      />
    </>
  );
};

export default SplashScreen;
