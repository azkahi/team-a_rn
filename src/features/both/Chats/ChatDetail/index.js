import React from 'react';
import { View, Text } from 'react-native';

import { styles } from './style';

const SplashScreen = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.label}>SplashScreen</Text>
    </View>
  );
};

export default SplashScreen;
