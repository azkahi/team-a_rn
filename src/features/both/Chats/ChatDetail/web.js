import React from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import { WebView } from 'react-native-webview';


const ChatDetail = (props) => {
  const [url, setUrl] = useState(
    'http://139.59.124.53:4581/api/chat?username=am_demo_1&account_id=21'
  );
  const INJECTEDJAVASCRIPT = 'const meta = document.createElement(\'meta\'); meta.setAttribute(\'content\', \'initial-scale=1, maximum-scale=1, width=device-width\'); meta.setAttribute(\'name\', \'viewport\'); document.getElementsByTagName(\'head\')[10].appendChild(meta); ';

  useEffect(() => {
    // console.log(props.route.params);
    setUrl(props.route.params);
  }, []);
  return (
    <WebView
      keyboardDisplayRequiresUserAction={true}
      pagingEnabled={false}
      showsHorizontalScrollIndicator={false}
      directionalLockEnabled={false}
      scalesPageToFit={false}
      useWebKit={true}
      injectedJavaScript={INJECTEDJAVASCRIPT}
      source={{
        uri: url
      }}
    />
  );
};

export default ChatDetail;
