import { StyleSheet } from 'react-native';
import { Colors } from 'utils/ColorReferences';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
    alignItems: 'center',
    backgroundColor: Colors.white700
  },
  judul: {
    color: Colors.red800,
    fontWeight: '500',
    fontSize: 28,
    fontFamily: 'Gill Sans',
    marginTop: 30
  },
  logo: {
    width: 116,
    height: 117,
    marginTop: 65
  },

  button: {
    alignSelf: 'center',
    width: 288,
    marginTop: 70,
    paddingVertical: 5,

    // backgroundColor: Colors.background,
    borderRadius: 5
  },
  form: {
    marginTop: 30
  },
  label: {
    color: 'white',
    fontWeight: '400',
    fontSize: 38
  },
  forgot: {
    color: Colors.grey800,
    textAlign: 'center',
    marginTop: 20
  },
  container1: {
    padding: 10,
    marginHorizontal: 5,
    marginVertical: 5,
    backgroundColor: Colors.red700,
    borderWidth: 2,
    borderColor: Colors.red700,
    borderRadius: 5
  },
  status: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center'
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center'
  }
});
