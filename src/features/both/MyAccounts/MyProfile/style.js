import { StyleSheet } from 'react-native';
import { Colors } from 'utils/ColorReferences';

export const styles = StyleSheet.create({
  mainPage: {
    backgroundColor: Colors.white700,
    flex: 1
  },
  topPage: {
    flex: 0.3,
    backgroundColor: Colors.red700
  },
  bottomPage: {
    flex: 0.7
  },
  profilePicture: {
    height: 150,
    width: 150,
    borderRadius: 100,
    alignSelf: 'center',
    marginTop: -80
  },
  profileContainer: {},
  profileRow: {
    flexDirection: 'row',
    marginHorizontal: 40
  },
  profileEmail: {
    alignSelf: 'center',
    marginVertical: 20,
    fontSize: 20,
    fontWeight: 'bold',
    color: Colors.red700
  },
  profileLabel: {
    flex: 0.7,
    alignSelf: 'flex-start',
    fontSize: 16,
    fontWeight: 'bold'
  },
  profileContent: {
    flex: 1,
    alignSelf: 'flex-start',
    fontSize: 16,
    color: Colors.red700
  },
  buttonEdit: {
    marginTop: 50,
    width: 250,
    borderRadius: 5,
    alignSelf: 'center'
  },
  buttonLogout: {
    marginTop: 10,
    width: 250,
    borderRadius: 5,
    alignSelf: 'center'
  }
});
