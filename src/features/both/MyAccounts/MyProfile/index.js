import React from 'react';
import { Text, View, Image } from 'react-native';
import Button from 'components/Button/Button';

import { useSelector, useDispatch } from 'react-redux';
import { reduxLogout } from 'reduxs/actions/AuthAction';
import { useEffect } from 'react';

import { styles } from './style';

const MyProfile = (props) => {
  const { session } = useSelector((state) => state.AuthReducer);
  const dispatch = useDispatch();
  const doLogout = () => dispatch(reduxLogout());

  useEffect(() => {
    // console.log(session);
    if (session.isLoggedIn === false) {
      props.navigation.reset({
        index: 0,
        routes: [{ name: 'Login' }]
      });
    }
  }, [session]);

  return (
    <View style={styles.mainPage}>
      <View style={styles.topPage} />
      <View style={styles.bottomPage}>
        <View>
          {/* <Image source={Images.profile} style={styles.profilePicture} /> */}
          <Image
            source={{ uri: session.data && session.data.avatar }}
            style={styles.profilePicture}
          />
        </View>
        <View style={styles.profileContainer}>
          {/* <View style={styles.profileRow}> */}
          <Text style={styles.profileEmail}>
            {session.data && session.data.email}
          </Text>
          {/* </View> */}
          <View style={styles.profileRow}>
            <Text style={styles.profileLabel}>Nama</Text>
            <Text style={styles.profileContent}>
              {': '}
              {session.data && session.data.name}
            </Text>
          </View>
          <View style={styles.profileRow}>
            <Text style={styles.profileLabel}>NIK</Text>
            <Text style={styles.profileContent}>
              {': '}
              {session.data && session.data.nik}
            </Text>
          </View>
          <View style={styles.profileRow}>
            <Text style={styles.profileLabel}>Title</Text>
            <Text style={styles.profileContent}>
              {': '}
              {session.data && session.data.job_position}
            </Text>
          </View>
          <View style={styles.profileRow}>
            <Text style={styles.profileLabel}>Phone Number</Text>
            <Text style={styles.profileContent}>
              {': '}
              {session.data && session.data.phone}
            </Text>
          </View>
          <View style={styles.profileRow}>
            <Text style={styles.profileLabel}>Workplace</Text>
            <Text style={styles.profileContent}>
              {': '}
              {session.data && session.data.workplace}
            </Text>
          </View>
        </View>
        <View style={styles.buttonEdit}>
          <Button
            title="Edit Profile"
            onPress={() => {
              props.navigation.navigate('EditProfile');
            }}
          />
        </View>
        <View style={styles.buttonLogout}>
          <Button
            type="secondary"
            title="Log Out"
            onPress={() => {
              doLogout();
            }}
          />
        </View>
      </View>
    </View>
  );
};

export default MyProfile;
