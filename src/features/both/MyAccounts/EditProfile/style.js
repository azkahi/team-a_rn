import { StyleSheet } from 'react-native';
import { Colors } from 'utils/ColorReferences';

export const styles = StyleSheet.create({
  scrollView: {
    flex: 1
  },
  scrollViewContainer: {
    flexGrow: 1,
    justifyContent: 'center'
  },
  mainPage: {
    backgroundColor: Colors.white700,
    flex: 1
  },
  topPage: {
    height: 200,
    backgroundColor: Colors.red700
  },
  bottomPage: {
    marginTop: -80
  },
  profilePicture: {
    height: 150,
    width: 150,
    borderRadius: 100,
    alignSelf: 'center'
  },
  profilePictureEdit: {
    position: 'absolute',
    borderRadius: 100,
    padding: 10,
    right: '30%',
    bottom: 2,
    backgroundColor: Colors.red700
  },
  profileContainer: {
    flexDirection: 'column'
  },
  profileRow: {
    marginHorizontal: 20,
    marginTop: 6
  },
  profileEmail: {
    alignSelf: 'center',
    marginVertical: 20,
    fontSize: 20,
    fontWeight: 'bold',
    color: Colors.red700
  },
  profileLabel: {
    flex: 0.7,
    alignSelf: 'flex-start',
    fontSize: 16,
    fontWeight: 'bold'
  },
  profileContent: {
    flex: 1,
    alignSelf: 'flex-start',
    fontSize: 16,
    color: Colors.red700
  },
  buttonSave: {
    marginVertical: 20,
    width: 250,
    borderRadius: 5,
    alignSelf: 'center'
  }
});
