import React from 'react';
import { TouchableOpacity } from 'react-native';
import { View, Text } from 'react-native';
import { SearchBar } from 'react-native-elements';

import CardChangePackageCorporate from 'components/CardChangePackageCorporate';

import { styles } from './style';


const OrderDetail = (props) => {
  const back = () => {
    props.navigation.navigate('OrderList');
  };


  return (
    <View>
      <View style = {styles.header}>
        <View style = {styles.container}>
          <Text style={styles.textRenewal}> Change Packeges</Text>
          <TouchableOpacity onPress={() => back()}>
            <Text style={styles.textgrey}> X</Text>
          </TouchableOpacity>
        </View>
        <SearchBar
          placeholder="Search Number"
        />
      </View>
      <TouchableOpacity onPress={() => {}}>
        <View>

          <CardChangePackageCorporate add_packege = "Add Packege" nomor = "628XXXXXX" paket ="OMG! 10GB" paket2 ="OMG! 20GB"/>
          <CardChangePackageCorporate add_packege = "Add Packege" nomor = "628XXXXXX" paket ="OMG! 10GB" paket2 ="OMG! 20GB"/>
          <CardChangePackageCorporate add_packege = "Add Packege" nomor = "628XXXXXX" paket ="OMG! 10GB" paket2 ="OMG! 20GB"/>
        </View>

      </TouchableOpacity>
      <View style={{ justifyContent: 'center', marginHorizontal: 30 }}>
        {/* <View style={{marginTop: 100}}>
    <Button2 title = "Change Selected" onPress={() => props.navigation.navigate('')} />
    </View>  */}
        {/* <View style={{marginTop: 10}}>
    <Button title = "Apply" onPress={() => props.navigation.navigate('')} />
   </View>  */}
      </View>
    </View>
  );
};


export default OrderDetail;
