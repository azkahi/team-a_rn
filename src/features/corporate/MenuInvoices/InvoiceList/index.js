import React, { useState, useEffect } from 'react';
import { View, FlatList, Alert, RefreshControl } from 'react-native';
import { useSelector } from 'react-redux';

import CardInvoiceCorporate from 'components/CardInvoiceCorporate/CardInvoiceCorporate';

import { getBillCorporate } from '../../../../engine/ApiProcessors';

import { styles } from './style';


const InvoicesCorporate = (props) => {
  const { session } = useSelector((state) => state.AuthReducer);
  const [billData, setBillData] = useState([]);
  const [refreshing] = useState(false);

  const getData = () => {
    getBillCorporate(session.token)
      .then((result) => {
        setBillData(result.data.data);
      })
      .catch((err) => {
        Alert.alert(`Error fetching MSISDN: ${err.toString()}`);
      });
  };

  useEffect(() => {
    getData();
  }, []);

  const BillDetail = (bill) => {
    props.navigation.navigate('InvoiceDetail', {
      bill
    });
  };

  const renderBillData = ({ item }) => {
    return (
      <CardInvoiceCorporate
        date={`${item.month}-${item.year}`}
        status={item.status_payment}
        amount={item.total_invoice}
        onPress={() => BillDetail(item)}
      />
    );
  };

  return (
    <View style={styles.mainPage}>
      <FlatList
        data={billData}
        keyExtractor={(item, index) => 'key' + index}
        renderItem={renderBillData}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={getData}
          />
        }
      />
    </View>
  );
};

export default InvoicesCorporate;
