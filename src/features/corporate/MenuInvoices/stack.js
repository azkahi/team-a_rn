import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { Colors } from 'utils/ColorReferences';

import InvoiceList from './InvoiceList';
import InvoiceDetail from './InvoiceDetail';

const Stack = createStackNavigator();

const StackNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: true,
        headerBackTitle: '',
        headerTintColor: Colors.white,
        headerStyle: {
          backgroundColor: Colors.red700,
          shadowColor: '#fff0'
        }
      }}
    >
      <Stack.Screen name="InvoiceList" component={InvoiceList} />
      <Stack.Screen name="InvoiceDetail" component={InvoiceDetail} />
    </Stack.Navigator>
  );
};

export default StackNavigator;
