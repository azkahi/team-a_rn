import { StyleSheet } from 'react-native';
import { Colors } from 'utils/ColorReferences';

export const styles = StyleSheet.create({
  mainPage: {
    backgroundColor: Colors.white700,
    flex: 1
  },
  textHeader: {
    color: Colors.red700,
    fontSize: 20,
    fontWeight: 'bold'
  },
  containerHeader: {
    backgroundColor: Colors.white700,
    justifyContent: 'space-around',
    borderBottomColor: Colors.gray700
  },
  cardTickets: {
    backgroundColor: Colors.white700,
    borderColor: Colors.gray700,
    borderWidth: 0.5,
    borderRadius: 5,
    margin: 20
  },
  company: {
    fontSize: 14,
    color: Colors.red900,
    fontWeight: 'bold',
    marginLeft: 10,
    marginTop: 5
  },
  status: {
    fontSize: 14,
    color: Colors.red900,
    fontWeight: 'bold',
    marginLeft: 10
  },
  category: {
    fontSize: 14,
    color: Colors.red900,
    marginLeft: 10
  },
  content: { marginLeft: 10, marginBottom: 10 },
  separator: {
    height: 0.5,
    backgroundColor: Colors.gray700,
    marginHorizontal: 20
  },
  rev1: {
    padding: 10,
    backgroundColor: Colors.white700,
    borderColor: Colors.gray700,
    borderWidth: 0.5,
    borderRadius: 20,
    marginHorizontal: 20,
    justifyContent: 'center',
    marginVertical: 10
  },
  textRev1: {
    color: 'black'
  },
  rev2: {
    padding: 10,
    backgroundColor: Colors.red700,
    borderColor: Colors.red700,
    borderWidth: 0.5,
    borderRadius: 20,
    justifyContent: 'center',
    marginHorizontal: 20,
    marginVertical: 10
  },
  textRev2: {
    color: 'white'
  },
  rev3: {
    backgroundColor: Colors.white700,
    borderColor: Colors.gray700,
    borderWidth: 0.5,
    borderRadius: 20,
    marginHorizontal: 20,
    marginTop: 5,
    marginBottom: 10,
    height: 35,
    justifyContent: 'center'
  },
  detail: {
    marginHorizontal: 20,
    marginBottom: 10
  },
  button: {
    width: 250,
    alignSelf: 'center',
    marginVertical: 20
  }
});
