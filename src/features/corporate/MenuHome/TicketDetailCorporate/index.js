import React, { useEffect, useState } from 'react';
import { Text, View, FlatList, Alert, TextInput } from 'react-native';
import { useSelector } from 'react-redux';

import Button from 'components/Button/Button';

import { getTicketDetailCorporate, replyTicketCorporate, closeTicketCorporate } from '../../../../engine/ApiProcessors';


import { styles } from './style';

const TicketDetailCorp = (props) => {
  const { session } = useSelector((state) => state.AuthReducer);
  const { ticket } = props.route.params;

  const [ticketDetailData, setTicketDetailData] = useState({ history_reply: [] });
  const [replyText, setReplyText] = useState('');

  const getTicketDetailData = () => {
    getTicketDetailCorporate(ticket.ticket_id, session.token)
      .then((result) => {
        setTicketDetailData(result.data.data);
      })
      .catch((err) => {
        Alert.alert(`Error getting ticket data: ${err.toString()}`);
      });
  };

  useEffect(() => {
    getTicketDetailData();
  }, []);

  const renderHistoryReply = ({ item }) => {
    return (
      <View style={item.office_name === 'Telkomsel' ? styles.rev2 : styles.rev1}>
        <Text style={item.office_name === 'Telkomsel' ? styles.textRev2 : styles.textRev1}>
          {item.comments}
        </Text>
      </View>
    );
  };

  const sendReply = (text) => {
    const data = {
      ticket_id: ticket.ticket_id,
      comments: text
    };

    replyTicketCorporate(data, session.token)
      .then((result) => {
        getTicketDetailData();
        setReplyText('');
      })
      .catch((err) => {
        Alert.alert(`Error replying ticket: ${err.toString()}`);
      });
  };

  const sendCloseTicketRequest = () => {
    const data = {
      ticket_id: ticket.ticket_id,
      action: ''
    };

    closeTicketCorporate(data, session.token)
      .then((result) => {
        Alert.alert('Ticket closed successfully');
        getTicketDetailData();
      })
      .catch((err) => {
        Alert.alert(`Error replying ticket: ${err.toString()}`);
      });
  };

  return (
    <View style={styles.mainPage}>
      <View style={styles.cardTickets}>
        <Text style={styles.company}>{ticketDetailData.account_name}</Text>
        <Text style={styles.status}>Status: {ticketDetailData.status}</Text>
        <Text style={styles.category}>{ticketDetailData.ticket_category}</Text>
        <Text style={styles.content}>
          {ticketDetailData.comment}
        </Text>
      </View>

      {
        ticketDetailData.status === 'open' ?
          <View style={styles.button}>
            <Button
              title="Close Ticket"
              onPress={() => {
                sendCloseTicketRequest();
              }}
              containerStyle={{}}
            />
          </View>
          : null
      }

      <View style={styles.separator} />

      <FlatList
        data={ticketDetailData.history_reply}
        renderItem={renderHistoryReply}
        keyExtractor={(item) => item.createdAt}
      />

      <View style={styles.detail}>
        <TextInput value={replyText} onChangeText={(text) => setReplyText(text)} />
      </View>
      <View style={styles.separator} />

      <View style={styles.button}>
        <Button
          title="Reply"
          onPress={() => {
            sendReply(replyText);
          }}
          containerStyle={{}}
        />
      </View>
    </View>
  );
};

export default TicketDetailCorp;
