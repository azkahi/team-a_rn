import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import ChatDetail from 'features/both/Chats/ChatDetail/web';

import { Colors } from 'utils/ColorReferences';

import Home from './Home';
import PopUpChangePacket from './PopUpChangePacket';
import PopUpCreateNewNumber from './PopUpCreateNewNumber';
import TicketDetailCorporate from './TicketDetailCorporate';

const Stack = createStackNavigator();

const StackNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: true,
        headerBackTitle: '',
        headerTintColor: Colors.white,
        headerStyle: {
          backgroundColor: Colors.red700,
          shadowColor: '#fff0'
        }
      }}
    >
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="PopUpChangePacket" component={PopUpChangePacket} />
      <Stack.Screen
        name="PopUpCreateNewNumber"
        component={PopUpCreateNewNumber}
      />
      <Stack.Screen
        name="TicketDetailCorporate"
        component={TicketDetailCorporate}
      />
      <Stack.Screen name="ChatDetail" component={ChatDetail} />
    </Stack.Navigator>
  );
};

export default StackNavigator;
