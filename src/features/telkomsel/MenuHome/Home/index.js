import React, { useEffect, useState } from 'react';
import {
  Text,
  Alert,
  FlatList,
  ScrollView,
  TouchableOpacity
} from 'react-native';
import CardTicket from 'components/CardTickets/CardTicket';
import CardBill from 'components/CardBills/CardBill';
import Icon from 'react-native-vector-icons/Ionicons';

import { useSelector } from 'react-redux';
import { Colors } from 'utils/ColorReferences';
import { Dimensions } from 'react-native';
import { BarChart } from 'react-native-chart-kit';

import { numberWithDots } from 'utils/helper';

import {
  getProfitHistoryAM,
  getTicketAM,
  getOutstandingBillsAM
} from 'engine/ApiProcessors';

import { styles } from './style';

const screenWidth = Dimensions.get('window').width;

export default function HomeAmScreen(props) {
  const { session } = useSelector((state) => state.AuthReducer);
  const [ticketData, setTicketData] = useState([]);
  const [billData, setBillData] = useState([]);
  const [chartData, setChartData] = useState({
    labels: ['January', 'February', 'March', 'April', 'May', 'June'],
    datasets: [
      {
        data: [20, 45, 28, 80, 99, 43]
      }
    ]
  });

  useEffect(() => {
    getProfitHistoryAM(session.token)
      .then((result) => {
        const tempData = result.data.data.slice(
          Math.max(result.data.data.length - 5, 0)
        );
        const labels = tempData.map((datum) => `${datum.month}-${datum.year}`);
        const data = tempData.map((datum) => datum.profit);
        setChartData({
          labels,
          datasets: [
            {
              data
            }
          ]
        });
      })
      .catch((err) => {
        Alert.alert(`Unable to fetch profit history data: ${err.toString()}`);
      });

    getTicketAM(session.token)
      .then((result) => {
        setTicketData(result.data.data);
      })
      .catch((err) => {
        Alert.alert(`Unable to fetch ticket data: ${err.toString()}`);
      });

    getOutstandingBillsAM(session.token)
      .then((result) => {
        setBillData(result.data.data);
      })
      .catch((err) => {
        Alert.alert(`Unable to fetch bills data: ${err.toString()}`);
      });
  }, []);

  const TicketDetail = (ticket) => {
    props.navigation.navigate('TicketDetail', {
      ticket
    });
  };

  const renderCardTicket = ({ item }) => {
    return (
      <CardTicket
        title={item.ticket_category}
        status={item.status}
        content={item.comment}
        onPress={() => TicketDetail(item)}
      />
    );
  };

  const CardBillDetail = (bill) => {
    props.navigation.navigate('CorporateBillsDetail', {
      bill
    });
  };

  const renderCardBill = ({ item }) => {

    return (
      <CardBill
        onPress={() => CardBillDetail(item)}
        total={numberWithDots(item.total_invoice)}
        companyName={item.account_name}
        disabled
      />
    );
  };

  return (
    <>
      <ScrollView contentContainerStyle={styles.container}>
        <Text style={styles.titleText}>Profit History</Text>
        <BarChart
          style={{
            padding: 20,
            backgroundColor: '#fff0'
          }}
          data={chartData}
          width={screenWidth - 40}
          height={200}
          chartConfig={{
            paddingRight: 20,
            paddingTop: 20,
            backgroundColor: '#fff0',
            backgroundGradientFrom: '#fff0',
            backgroundGradientTo: '#fff0',
            fillShadowGradient: Colors.red700,
            fillShadowGradientOpacity: 1,
            decimalPlaces: 0,
            color: (opacity = 1) => Colors.red700,
            labelColor: (opacity = 1) => Colors.red700,
            style: {
              borderRadius: 16,
              padding: 20
            }
          }}
          fromZero={true}
        />
        <Text style={styles.titleText}>Tickets</Text>
        <FlatList
          data={ticketData}
          renderItem={renderCardTicket}
          keyExtractor={(item) => item.ticket_id.toString()}
          horizontal
          style={styles.flatList}
        />
        <Text style={styles.titleText}>Bills</Text>
        <FlatList
          data={billData}
          renderItem={renderCardBill}
          keyExtractor={(item) => item.account_id.toString()}
          horizontal
          style={styles.flatList}
        />
      </ScrollView>
      <TouchableOpacity
        style={{
          width: 58,
          height: 58,
          borderWidth: 0.5,
          borderColor: Colors.red700,
          alignItems: 'center',
          justifyContent: 'center',
          position: 'absolute',
          bottom: 16,
          right: 18,
          backgroundColor: '#fff',
          borderRadius: 100
        }}
        onPress={() => {
          props.navigation.navigate('ChatList');
        }}
      >
        <Icon name="chatbubbles-outline" size={30} color={Colors.red700} />
      </TouchableOpacity>
    </>
  );
}
