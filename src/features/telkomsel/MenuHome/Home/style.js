import { StyleSheet } from 'react-native';
import { Colors } from 'utils/ColorReferences';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff0'
  },
  contentContainer: {
    flex: 1,
    backgroundColor: Colors.background,
    paddingHorizontal: 18,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20
  },
  headerText: {
    color: Colors.white,
    fontSize: 20
  },
  profileText: {
    fontSize: 24,
    marginTop: 20,
    color: Colors.white
  },
  button: {
    marginTop: 30,
    alignSelf: 'center',
    backgroundColor: Colors.white,
    borderRadius: 8
  },
  buttonText: {
    fontSize: 20,
    color: Colors.black
  },
  editProfPicButton: {
    padding: 5,
    borderRadius: 60,
    backgroundColor: Colors.colorSecondaryDark,
    position: 'absolute',
    right: 5,
    bottom: 5
  },
  textInput: {
    borderBottomWidth: 1,
    borderColor: Colors.white,
    width: '85%',
    color: Colors.white,
    fontSize: 24
  },
  titleText: {
    textAlign: 'center',
    color: Colors.red700,
    fontSize: 24,
    marginVertical: 10,
    marginTop: 30
  },
  flatList: {
    marginVertical: 10
  },
  chatButton: {
    height: 50,
    width: 50,
    borderRadius: 50,
    borderWidth: 0.3,
    position: 'absolute',
    bottom: 10,
    right: 10,
    borderColor: Colors.gray700
  },
  icon: {
    alignSelf: 'center',
    marginVertical: 12
  }
});
