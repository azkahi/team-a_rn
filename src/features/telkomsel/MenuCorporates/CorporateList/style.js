import { StyleSheet } from 'react-native';
import { Colors } from 'utils/ColorReferences';

export const styles = StyleSheet.create({
  header: {
    fontSize: 28,
    textAlign: 'center'
  },
  container: {
    flexDirection: 'row',
    padding: 10,
    marginHorizontal: 5,
    marginVertical: 5,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: Colors.grey700,
    borderRadius: 10
  },
  Corporate: {
    justifyContent: 'center',
    marginLeft: 20
  },
  textWrapper: {
    justifyContent: 'center',
    marginLeft: 20,
    marginBottom: 50
  },
  textgrey: {
    color: Colors.grey700,
    fontSize: 17,
    marginLeft: 5
  },
  textred: {
    color: Colors.red700,
    fontSize: 17,
    marginBottom: 25
  },
  image: {
    width: 84,
    height: 84,
    borderRadius: 84 / 2,
    marginLeft: 2
  },
  pic: {
    textAlign: 'center',
    color: Colors.red700,
    fontSize: 17,
    marginBottom: 15
  }
});
