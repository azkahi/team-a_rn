import React, { useState, useEffect } from 'react';
import { Text, View, FlatList, Alert } from 'react-native';
import { useSelector } from 'react-redux';

import { numberWithDots } from 'utils/helper';

import { getBillDetailAM, getPackage } from '../../../../engine/ApiProcessors';

import { styles } from './style';

const BillDetailAM = (props) => {
  const { bill } = props.route.params;
  const { session } = useSelector((state) => state.AuthReducer);

  const [billDetailData, setBillDetailData] = useState([]);
  const [packageData, setPackageData] = useState([]);

  const getBillData = () => {
    getBillDetailAM(bill.billing_id, session.token)
      .then((result) => {
        setBillDetailData(result.data.data);
      })
      .catch((err) => {
        Alert.alert(`Error fetching bill detail: ${err.toString()}`);
      });
  };

  useEffect(() => {
    getBillData();

    getPackage(session.token)
      .then((result) => {
        setPackageData(result.data.data);
      })
      .catch((err) => {
        Alert.alert(`Error fetching package data: ${err.toString()}`);
      });
  });

  const renderBillDetail = ({ item }) => {
    const packageItem = packageData.find((packageDatum) => packageDatum.package_id === item.package_id);

    return (
      <>
        <View style={styles.row}>
          <View>
            <Text style={styles.pack}>{ packageItem ? packageItem.package_name : null}</Text>
            <Text style={styles.pcs}>{item.package_qty} pcs</Text>
          </View>
          <Text style={styles.amountDetail}>Rp. {numberWithDots(item.package_total)}</Text>
        </View>
      </>
    );
  };

  return (
    <View style={styles.mainPage}>
      <Text style={styles.summary}>Summary</Text>
      <Text style={styles.status}>Status: {bill.status_payment}</Text>
      <Text style={styles.month}>{`${bill.month}-${bill.year}`}</Text>
      <Text style={styles.amount}>Total Rp. {bill.total_invoice ? numberWithDots(bill.total_invoice) : 0}</Text>

      <Text style={styles.detail}>Details</Text>
      <FlatList
        data={billDetailData}
        keyExtractor={(_, index) => index.toString()}
        renderItem={renderBillDetail}
      />
    </View>
  );
};

export default BillDetailAM;
