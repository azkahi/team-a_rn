import { StyleSheet } from 'react-native';
import { Colors } from 'utils/ColorReferences';

export const styles = StyleSheet.create({
  textHeader: {
    color: Colors.red700,
    fontSize: 20,
    fontWeight: 'bold'
  },
  containerHeader: {
    backgroundColor: Colors.white700,
    justifyContent: 'space-around',
    borderBottomColor: Colors.gray700
  },
  picCorporate: {
    flexDirection: 'row',
    backgroundColor: Colors.white700,
    padding: 20
  },
  profilePicturePosition: {
    alignSelf: 'center'
  },
  profilePicture: {
    height: 80,
    width: 80,
    borderRadius: 100
  },
  detailProfile: {
    marginLeft: 20,
    alignSelf: 'center',
    marginBottom: 10
  },
  picPosition: {
    alignSelf: 'center',
    alignItems: 'center'
  },
  pic: {
    fontSize: 14,
    color: Colors.red700,
    fontWeight: 'bold',
    marginVertical: 10
  },
  name: {
    fontSize: 14,
    color: Colors.gray700,
    fontWeight: '400'
  },
  email: {
    fontSize: 14,
    color: Colors.gray700,
    fontWeight: '400'
  },
  phone: {
    fontSize: 14,
    color: Colors.gray700,
    fontWeight: '400'
  },
  button: {
    width: 250,
    alignSelf: 'center',
    marginTop: 10
  },
  billsText: {
    fontSize: 16,
    color: Colors.red700,
    fontWeight: 'bold',
    marginTop: 20,
    marginBottom: 10,
    marginLeft: 20
  },
  billing: {
    flexDirection: 'row'
  },
  ticketsText: {
    fontSize: 16,
    color: Colors.red700,
    fontWeight: 'bold',
    marginTop: 20,
    marginBottom: 10,
    marginLeft: 20
  },
  tickets: {
    flexDirection: 'row'
  },
  layar: {
    height: '80%',
    backgroundColor: 'white',
    marginVertical: 60,
    marginHorizontal: 35,
    width: '80%',
    display: 'flex',
    borderRadius: 20,
    padding: 10
  },
  layar2: {
    backgroundColor: 'white',
    marginVertical: 60,
    marginHorizontal: 35,
    width: '80%',
    display: 'flex',
    borderRadius: 20,
    borderWidth: StyleSheet.hairlineWidth
  },
  layar3: {
    height: '80%',
    backgroundColor: 'white',
    marginVertical: 60,
    marginHorizontal: 35,
    width: '80%',
    display: 'flex',
    borderRadius: 20,
    padding: 10
  },
  textgrey: {
    color: Colors.gray700,
    fontSize: 21,
    fontWeight: 'bold',
    marginLeft: 50
  },
  container: {
    flexDirection: 'row',
    padding: 15,
    justifyContent: 'flex-end'
  },
  textRenewal: {
    color: Colors.red700,
    fontSize: 20,
    fontWeight: 'bold',
    marginLeft: 20,
    marginVertical: 10
  },
  textRenewal1: {
    color: Colors.red700,
    fontSize: 20,
    fontWeight: 'bold',
    marginLeft: 90
  },
  pickerContainer: {
    borderWidth: 1,
    margin: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  picker: {
    width: '100%'
  },
  textInputContainer: {
    borderWidth: 1,
    margin: 10
  },
  textInput: {
  }
});

