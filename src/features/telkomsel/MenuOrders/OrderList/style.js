import { StyleSheet } from 'react-native';
import { Colors } from 'utils/ColorReferences';

export const styles = StyleSheet.create({
  header: {
    fontSize: 28,
    textAlign: 'center'
  },
  container: {
    flexDirection: 'row',
    padding: 10,
    marginHorizontal: 5,
    marginVertical: 5,

    borderWidth: 2,
    borderColor: '#8492A6',
    borderRadius: 10
  },
  Corporate: {
    justifyContent: 'center',
    marginLeft: 20
  },
  textWrapper: {
    justifyContent: 'center',
    marginLeft: 20,
    marginBottom: 50
  },
  textred: {
    color: Colors.red700,
    fontSize: 17,
    marginBottom: 1
  },
  textgrey: {
    color: Colors.gray700,
    fontSize: 17,
    marginLeft: 5
  },
  textRenewal: {
    color: Colors.red700,
    fontSize: 17,
    fontWeight: 'bold',
    marginVertical: 5
  },
  textStatus: {
    textAlign: 'center',
    color: Colors.red700,
    fontSize: 17,
    marginTop: 40,
    fontWeight: 'bold',
    marginLeft: 40
  }
});
