import React, { useEffect, useState } from 'react';
import { Alert } from 'react-native';
import { useSelector } from 'react-redux';
import { View, Text, FlatList, TouchableOpacity } from 'react-native';
import { SearchBar } from 'react-native-elements';

import CardOrderDetail from 'components/CardOrderDetail/CardOrderDetail';

import { getOrderItemAM, approveRejectOrderItemAM, approveRejectOrderAM, getPackage } from '../../../../engine/ApiProcessors';

import { styles } from './style';

const OrderDetailScreen = (props) => {
  const { session } = useSelector((state) => state.AuthReducer);
  const { orderId } = props.route.params;
  const [orderItemData, setOrderItemData] = useState([]);
  const [initialOrderItemData, setInitialOrderItemData] = useState([]);
  const [enableApproveAll, setEnableApproveAll] = useState(true);
  const [searchText, setSearchText] = useState('');
  const [packageData, setPackageData] = useState([]);

  const getOrderItemData = () => {
    getOrderItemAM(orderId, session.token)
      .then((result) => {
        setInitialOrderItemData(result.data.data);
        setOrderItemData(result.data.data);
        const isOpenAll = result.data.data.every((data) => data.status === 'open');
        setEnableApproveAll(isOpenAll);
      })
      .catch((err) => {
        Alert.alert(err.toString());
      });
  };

  useEffect(() => {
    getOrderItemData();
    getPackage(session.token)
      .then((result) => {
        setPackageData(result.data.data);
      })
      .catch((err) => {
        Alert.alert(`Error fetching package data: ${err.toString()}`);
      });
  }, []);

  const approveRejectOrderItem = (isApprove, itemId) => {
    const data = [
      { status: isApprove ? 'completed' : 'canceled', order_item_id: itemId }
    ];

    approveRejectOrderItemAM(data, session.token)
      .then((result) => {
        Alert.alert(`Order ${isApprove ? 'approved' : 'rejected'} successfully.`);
        getOrderItemData();
      })
      .catch((err) => {
        Alert.alert(err.toString());
      });
  };

  const approveRejectOrder = (isApprove) => {
    const data = { status: isApprove ? 'completed' : 'canceled', order_id: orderId };

    approveRejectOrderAM(data, session.token)
      .then((result) => {
        Alert.alert(`Order ${isApprove ? 'approved' : 'rejected'} successfully.`);
        getOrderItemData();
      })
      .catch((err) => {
        Alert.alert(err.toString());
      });
  };

  const renderCardOrderDetail = ({ item }) => {
    let packageName = '';
    packageData.forEach((paket) => {
      if (item.package_id === paket.package_id) {
        packageName = paket.package_name;
      }
    });

    return (
      <CardOrderDetail
        status={item.status}
        renewal={item.renewal ? 'Renewal' : ''}
        nomor={item.msisdn}
        orderType={item.order_type}
        paket={packageName}
        onPressApprove={() => approveRejectOrderItem(true, item.id)}
        onPressReject={() => approveRejectOrderItem(false, item.id)}
      />
    );
  };

  const searchNumbers = (text) => {
    setSearchText(text);
    if (text) {
      setOrderItemData(initialOrderItemData.filter((orderItem) => {
        return orderItem.msisdn.search(text) >= 0;
      }));
    } else {
      setOrderItemData(initialOrderItemData);
    }
  };

  return (
    <View style={{ flex: 1 }}>
      <View style={styles.header}>
        <SearchBar placeholder="Search Number" value={searchText} onChangeText={(text) => searchNumbers(text)} />
      </View>
      { enableApproveAll ?
        <View style={styles.approveRejectButtons}>
          <TouchableOpacity style={styles.container3} onPress={() => approveRejectOrder(true)}>
            <Text style={styles.status}>Approve All</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.container3} onPress={() => approveRejectOrder(false)}>
            <Text style={styles.status}>Reject All</Text>
          </TouchableOpacity>
        </View>
        : null
      }
      <FlatList
        data={orderItemData}
        renderItem={renderCardOrderDetail}
        keyExtractor={(item) => item.id.toString()}
      />
    </View>
  );
};

export default OrderDetailScreen;
