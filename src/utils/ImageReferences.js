export class Images {
  static app_icon = require('assets/images/app_icon.png');
  static logo = require('assets/images/logo.png');
  static backgroundlogin = require('assets/images/Background.jpg');
  static profile = require('assets/images/avatar.jpg');
  static edit = require('assets/images/icon_edit.png');
  static trash = require('assets/images/icon_trash.png');
}
