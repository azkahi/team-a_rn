import Color from 'react-native-material-color';
export class Colors {
  // Google Material Colors
  static red700 = Color.RED[700];
  static red800 = Color.RED[800];
  static red900 = Color.RED[900];

  static white700 = Color.WHITE[700];
  static white800 = Color.WHITE[800];
  static white900 = Color.WHITE[900];

  static gray700 = Color.GREY[700];
  static gray800 = Color.GREY[800];
  static gray900 = Color.GREY[900];

  static white = '#FFF';
  static black = '#111';
}
