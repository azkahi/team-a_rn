import React from 'react';

import { View, Text, TouchableOpacity } from 'react-native';

import { styles } from './CardOrderStyle';



const CardOrder = ({ date, Corporate, Nama, Email, Phone, Renewal, Status, quantity, packageName, addNumber, onPressApprove, onPressReject }) => {
  return (
    <View>
      <View style={styles.container}>
        <View>
          <Text style={styles.textStatus}> Status: {Status}</Text>
          <Text style={styles.textred}> {date}</Text>
          <Text style={styles.textred}> {Corporate}</Text>
          <Text style={styles.textgrey}> {Nama}</Text>
          <Text style={styles.textgrey}> {Email}</Text>
          <Text style={styles.textgrey}> {Phone}</Text>
          <Text style={styles.textgrey}> {Phone}</Text>
          <Text style={styles.textgrey}> Quantity: {quantity}</Text>
          { packageName !== '' ? <Text style={styles.textgrey}> {packageName}</Text> : null }
          <Text style={styles.textRenewal}> {Renewal}</Text>

          { addNumber ?
            <View style={styles.approveRejectButtons}>
              <TouchableOpacity style={styles.container3} onPress={onPressApprove}>
                <Text style={styles.status}>Approve</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.container3} onPress={onPressReject}>
                <Text style={styles.status}>Reject</Text>
              </TouchableOpacity>
            </View>
            : null
          }
        </View>
      </View>
    </View>
  );
};


export default CardOrder;

