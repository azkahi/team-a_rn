import React from 'react';
import { View, Text, Image } from 'react-native';
import { Images } from 'utils/ImageReferences';

import { styles } from './ChangePackegesStyle';

const ChangePackeges = ({ nomor, paket, paket2, add_packege, Status }) => {
  return (
    <View>
      <View style={styles.container}>
        <View>
          <Text style={styles.textRenewal}> {nomor}</Text>
        </View>
        <View style={styles.container2}>
          <View style={styles.container1}>
            <Text style={styles.status}> {paket}</Text>
            <Image style={styles.edit} source={Images.edit} />
            <Image style={styles.trash} source={Images.trash} />
          </View>
          <View style={styles.container1}>
            <Text style={styles.status}> {paket2}</Text>
            <Image style={styles.edit} source={Images.edit} />
            <Image style={styles.trash} source={Images.trash} />
          </View>
        </View>
        <View style={styles.container3}>
          <Text style={styles.status}> {add_packege}</Text>
        </View>
      </View>
    </View>
  );
};

export default ChangePackeges;
