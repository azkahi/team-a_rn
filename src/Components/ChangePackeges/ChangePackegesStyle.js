import { StyleSheet } from 'react-native';
import { Colors } from 'utils/ColorReferences';

export const styles = StyleSheet.create({
  //   header: {
  //       marginTop: 60,
  //       fontSize: 28,
  //       textAlign: 'center',

  // },
  container: {



    marginHorizontal: 2,
    marginVertical: 5,
    padding: 2,
    borderWidth: 1,
    borderColor: Colors.gray700,
    borderRadius: 20,


    justifyContent: 'center'
  },
  Corporate: {

    justifyContent: 'center',
    marginLeft: 20
  },
  textWrapper: {
    justifyContent: 'center',
    marginLeft: 50,
    marginBottom: 20
  },

  textred:
  {
    color: Colors.red700,
    fontSize: 17,
    marginBottom: 10

  },
  textgrey:
  {
    color: Colors.gray700,
    fontSize: 17,
    marginLeft: 5
  },
  textRenewal:
  {
    color: Colors.red700,
    fontSize: 17,
    fontWeight: 'bold',
    marginVertical: 5
  },
  textStatus:
  {
    textAlign: 'center',
    color: Colors.red700,
    fontSize: 17,
    marginTop: 1,
    fontWeight: 'bold'
  },
  textred1:
  {
    color: Colors.red700,
    fontSize: 17,
    textAlign: 'center',
    marginVertical: 20


  },
  container1: {

    flexDirection: 'row',
    padding: 5,
    marginHorizontal: 5,
    marginVertical: 5,
    backgroundColor: Colors.red700,
    borderWidth: 2,
    borderColor: Colors.red700,
    borderRadius: 5,
    width: 139,
    height: 34,
    justifyContent: 'center'

  },
  status: {
    color: 'white',
    fontWeight: 'bold',
    marginLeft: 1

  },
  container2: {

    flexDirection: 'row',
    padding: 10,
    borderRadius: 10
  },
  container3: {
    padding: 10,
    marginHorizontal: 100,


    backgroundColor: Colors.red700,
    borderWidth: 2,
    borderColor: Colors.red700,
    borderRadius: 5,
    width: 118,
    height: 41
  },

  edit: {
    width: 10,
    height: 10,
    marginLeft: 15

  },
  trash: {
    width: 17,
    height: 17,
    marginLeft: 5

  }


});
