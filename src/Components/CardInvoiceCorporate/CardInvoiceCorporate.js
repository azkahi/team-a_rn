import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { numberWithDots } from 'utils/helper';

import { Colors } from '../../utils/ColorReferences';


import { styles } from './CardInvoiceCorporateStyle';

const CardInvoiceCorporate = ({ date, status, amount, onPress }) => {
  return (
    <TouchableOpacity style={styles.cardInvoice} onPress={onPress}>
      <View style={styles.invoiceWrap}>
        <Text style={styles.month}>{date}</Text>
        <Text style={styles.amount}>{status} Bill</Text>
        <Text style={styles.amount}>Total: Rp. {numberWithDots(amount)}</Text>
      </View>
      <View style={styles.statusWrap}>
        <TouchableOpacity>
          <Icon
            name="chevron-right"
            style={styles.icon}
            size={25}
            color={Colors.gray700}
          />
        </TouchableOpacity>
      </View>
    </TouchableOpacity>
  );
};

export default CardInvoiceCorporate;
