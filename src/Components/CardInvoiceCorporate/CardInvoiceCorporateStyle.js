import { StyleSheet } from 'react-native';
import { Colors } from 'utils/ColorReferences';

export const styles = StyleSheet.create({
  cardInvoice: {
    backgroundColor: Colors.white700,
    borderColor: Colors.gray700,
    borderWidth: 0.5,
    borderRadius: 5,
    marginHorizontal: 15,
    marginTop: 10,
    flexDirection: 'row'
  },

  invoiceWrap: {
    marginLeft: 10,
    marginVertical: 10
  },
  month: {
    color: Colors.red900,
    fontWeight: 'bold'
  },
  amount: {
    fontWeight: 'bold',
    color: Colors.gray700
  },
  statusWrap: {
    alignSelf: 'center',
    marginLeft: 80,
    flexDirection: 'row'
  },
  icon: {
    marginLeft: 100
  }
});
