import { StyleSheet } from 'react-native';
import { Colors } from 'utils/ColorReferences';

export const styles = StyleSheet.create({
  header: {
    marginTop: 60,
    fontSize: 28,
    textAlign: 'center'
  },
  container: {
    flexDirection: 'column',
    padding: 15,
    marginHorizontal: 5,
    marginVertical: 5,
    borderWidth: 1,
    borderColor: Colors.grey700,
    borderRadius: 10
  },
  Corporate: {
    justifyContent: 'center',
    marginLeft: 20
  },
  textWrapper: {
    flex: 1
  },
  textgrey: {
    color: Colors.grey700,
    fontSize: 17,
    flex: 1,
    flexWrap: 'wrap',
    marginVertical: 5
  },
  textred: {
    color: Colors.red700,
    fontSize: 17,
    fontWeight: 'bold',
    marginBottom: 25
  },
  image: {
    width: 84,
    height: 84,
    borderRadius: 84 / 2,
    margin: 10,
    marginRight: 40,
    alignSelf: 'center'
  },
  pic: {
    textAlign: 'center',
    fontWeight: 'bold',
    color: Colors.red700,
    fontSize: 17,
    marginBottom: 10
  },
  containerContent: {
    flexDirection: 'row'
  }
});
