import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

import { styles } from './style';

const CardTicketCorporate = ({ title, status, content, onPress }) => {
  return (
    <TouchableOpacity style={styles.ticket} onPress={onPress}>
      <Text style={styles.title}>{title}</Text>
      <Text style={styles.subtitle}>Status: {status}</Text>
      <Text>{content}</Text>
    </TouchableOpacity>
  );
};

export default CardTicketCorporate;
