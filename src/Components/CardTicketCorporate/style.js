import { StyleSheet } from 'react-native';
import { Colors } from 'utils/ColorReferences';

export const styles = StyleSheet.create({
  ticket: {
    borderWidth: 0.2,
    borderRadius: 10,
    width: 150,
    marginLeft: 20,
    marginTop: 20,
    padding: 15
  },
  title: {
    fontSize: 16,
    color: Colors.red700,
    fontWeight: 'bold'
  },
  subtitle: {
    fontSize: 14,
    color: Colors.gray700,
    fontWeight: 'bold'
  }
});
