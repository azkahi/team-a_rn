import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

import { styles } from './ButtonStyle';

const Button2 = ({ title, onPress }) => {
  return (
    <TouchableOpacity style={styles.button} onPress={onPress}>
      <Text style={styles.textButton}>{title}</Text>
    </TouchableOpacity>
  );
};

export default Button2;
