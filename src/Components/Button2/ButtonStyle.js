import { StyleSheet } from 'react-native';
import { Colors } from 'utils/ColorReferences';

export const styles = StyleSheet.create({
  button: {
    backgroundColor: '#E5E9F2',
    borderRadius: 5
  },
  textButton: {
    fontSize: 16,

    textAlign: 'center',
    color: Colors.gray700,
    paddingVertical: 12,
    alignSelf: 'center',
    lineHeight: 18
  }
});
