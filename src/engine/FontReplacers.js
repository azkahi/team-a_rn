import React from 'react';
import { Text, TextInput, Platform } from 'react-native';
// import {TextInput} from 'react-native-gesture-handler';

export const FontReplacers = (font) => {
  Platform.select({
    ios: font,
    android: `${font}-Regular`
  });

  let item = [Text, TextInput];
  item.forEach((v, i) => {
    let renderer = v.render;
    v.render = function (...args) {
      const origin = renderer.call(this, ...args);
      return React.cloneElement(origin, {
        // style: [styles.defaultFont, origin.props.style],
        style: [{ fontFamily: `${font}-Regular` }, origin.props.style]
      });
    };
  });
};

// const styles = StyleSheet.create({
//   defaultFont: {
//     fontFamily: 'NunitoSans-Regular',
//   },
// });
